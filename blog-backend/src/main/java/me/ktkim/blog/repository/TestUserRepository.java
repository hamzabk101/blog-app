package me.ktkim.blog.repository;

import me.ktkim.blog.model.domain.Ftable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TestUserRepository extends JpaRepository<Ftable,Integer> {

}
